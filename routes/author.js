const express = require('express')
const router = express.Router();
const authorController = require('../controllers/authorController')

router.get('/', authorController.getAuthorAll);
router.get('/:id', authorController.getAuthorByID);
router.post('/', authorController.createAuthor);
router.put('/:id', authorController.updateAuthor);
router.delete('/:id', authorController.deleteAuthor);

module.exports = router;

var express = require('express');
var router = express.Router();
const indexController = require('../controllers/indexController')

/* GET home page. */
// router.get('/', indexController.GetLandingpage);
router.get('/', indexController.GetLandingPage);

module.exports = router;

const { Author, Book } = require('../models')

exports.getAuthorAll = (req,res) => {
    console.log('Im here')
    Author.findAll({
        include: [
            {
                model: Book,
                as: 'books'
            }
        ]
    }).then((author) => {
        res.status(200).json({
            status: true,
            message: 'Authors retrieved',
            data: author
        })
    }).catch((e) => {
        res.status(400).json({
            status: false,
            message: 'Error'
        })
    })
}

exports.getAuthorByID = (req,res) => {
    Author.findByPk(req.params.id).then((author) => {
        res.status(200).json({
            status: true,
            message: `author with ${req.params.id} retrieved`,
            data: author
        })
    }).catch((e) => {
        res.status(400).json({
            status: false,
            message: 'Error'
        })
    })
}

exports.createAuthor = (req,res) => {    
    Author.create({
        name: req.body.name,
        country: req.body.country,
        birthYear: req.body.birthYear
    }).then((author) => {
        res.status(200).json({
            status: true,
            message: `Author is created`,
            data: author
        })
    })
}

exports.updateAuthor = (req,res) => {
    Author.findByPk(req.params.id).then((author) => {
        author.update({
            name: req.body.name,
            country: req.body.country,
            birthYear: req.body.birthYear
        }, {
            where : {
                id : req.params.id
            }
        }).then(() => {
            res.status(200).json({
                status: true,
                message: `Author with ID ${req.params.id} is updated`,
                data: author
            })
        })
    })
}

exports.deleteAuthor = (req,res) => {
    Author.destroy({
        where : {
            id : req.params.id
        }
    }).then(() => {
        res.status(301).json({
            status: true,
            message: `Author with ID ${req.params.id} deleted`,
        })
    })
}


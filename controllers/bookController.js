const { Author,Book } = require('../models')

exports.getBookAll = (req,res) => {
    Book.findAll({
        include: [
            {
                model: Author,
                as: 'author'
            }
        ]
    }).then((book) => {
        res.status(200).json({
            status: true,
            message: 'Books retrieved',
            data: book
        })
    }).catch((e) => {
        console.log(e)
        res.status(400).json({
            status: false,
            message: 'Error'
        })
    })
}

exports.getBookByID = (req,res) => {
    Book.findByPk(req.params.id, {
        include: [
            {
            model: Author,
            as: 'author'
            }
        ]
    }).then((book) => {
        res.status(200).json({
            status: true,
            message: `book with ${req.params.id} retrieved`,
            data: book
        })
    }).catch((e) => {
        res.status(400).json({
            status: false,
            message: 'Error'
        })
    })
}

exports.createBook = (req,res) => {
        Book.create({
        title: req.body.title,
        genre: req.body.genre,
        authorId: req.body.authorId
    }).then((book) => {
        res.status(200).json({
            status: true,
            message: `Book is created`,
            data: book
        })
    })
}

exports.updateBook = (req,res) => {
    Book.findByPk(req.params.id).then((book) => {
        book.update({
            title: req.body.title,
            genre: req.body.genre,
            authorId: req.body.authorId
        }, {
            where : {
                id : req.params.id
            }
        }).then(() => {
            res.status(200).json({
                status: true,
                message: `Book with ID ${req.params.id} is updated`,
                data: book
            })
        })
    })
}

exports.deleteBook = (req,res) => {
    Book.destroy({
        where : {
            id : req.params.id
        }
    }).then(() => {
        res.status(301).json({
            status: true,
            message: `Book with ID ${req.params.id} deleted`,
        })
    })
}





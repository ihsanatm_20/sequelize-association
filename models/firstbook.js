'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class firstBook extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    //   firstBook.belongsTo(models.Author, { foreignKey: 'author_id' })
    }
  };

  firstBook.init({
    title: DataTypes.STRING,
    genre: DataTypes.STRING,
    page_amount: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'firstBook',
  });
  return firstBook;
};